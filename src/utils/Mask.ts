interface MaskToken {
    pattern?: RegExp;
    transform?: (v: string) => string;
    avoid?: boolean;
}

export interface MaskTokens {
    [key: string]: MaskToken,
}

export const DEFAULT_MASK_TOKENS: MaskTokens = {
    'F': {
        pattern: /[a-zA-Z0-9а-яёА-ЯЁ_~№!@#$%^&*"()\-+=[\]{}|\\/.,<>?;:\s]/,
    },
    'L': {pattern: /[a-zA-Z0-9#_]/},
    'd': {pattern: /\d/},
    'X': {pattern: /[0-9a-zA-Z]/},
    'S': {pattern: /[a-zA-Z]/},
    'U': {pattern: /[a-zA-Z]/, transform: v => v.toLocaleUpperCase()},
    'l': {pattern: /[a-zA-Z]/, transform: v => v.toLocaleLowerCase()},
    '!': {avoid: true},
    'C': {pattern: /[\w\d\W]/},
    '%': {pattern: /\d|\./},
    'P': {pattern: /[a-zA-Zа-яёА-ЯЁ0-9`~!@#$%\^&*()_\-+={}\[\]\\|:;"'<>,\.\?\/]/},
};

export default class Mask {
    static apply(value: string, mask: string | string[], maskTokens?: MaskTokens) {
        const tokens = maskTokens ?? DEFAULT_MASK_TOKENS;

        if (Array.isArray(mask)) {
            return this.applyMultipleMasks(value, mask, tokens);
        } else {
            return this.applyMask(value, mask, tokens);
        }
    }

    static applyMultipleMasks(value: string, masks: string[], tokens?: MaskTokens) {
        const sortedMasks = masks.sort((a, b) => a.length - b.length);

        for (let i = 0; i < sortedMasks.length; i++) {
            const currentMask = sortedMasks[i];
            const nextMask = sortedMasks[i + 1];
            let maskedValue = '';

            if (nextMask) {
                maskedValue = Mask.applyMask(value, nextMask, tokens);
            }

            if (maskedValue.length < currentMask.length) {
                return Mask.applyMask(value, currentMask, tokens);
            }
        }

        return '';
    }

    static applyMask(value: string = '', mask: string, maskTokens?: MaskTokens) {
        const tokens = maskTokens ?? DEFAULT_MASK_TOKENS;
        let maskedValue = '';
        let ending = '';

        for (let i = 0, j = 0; i < mask.length;) {
            const cMask = mask[i];
            const cValue = value[j];
            const token = tokens[cMask];

            if (j >= value.length) {
                if (tokens[cMask]) {
                    ending = '';
                    break;
                }

                ending += cMask;
                i++;
            } else if (token?.avoid && mask[i + 1]) {
                maskedValue += mask[++i];
                i++;
            } else if (token?.pattern) {
                if (token.pattern.test(cValue)) {
                    maskedValue += token.transform ? token.transform(cValue) : cValue;
                    i++;
                }

                j++;
            } else {
                maskedValue += cMask;

                if (cValue === cMask) {
                    j++;
                }

                i++;
            }
        }

        const output = `${maskedValue}${ending}`;

        if (output.trim().length) {
            return output;
        }

        return '';
    }
}
