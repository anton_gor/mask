import type {Directive} from 'vue';
import MaskHelper, {DEFAULT_MASK_TOKENS, type MaskTokens} from '../utils/Mask';

export interface VMaskConfig {
    mask: string | string[];
    tokens: MaskTokens;
}

export const vMask: Directive<HTMLInputElement | HTMLElement, string | string[] | VMaskConfig> = (el, binding) => {
    let inputEl = getInputElement(el);
    let config = getConfig(binding.value);

    addOnInputHandler(inputEl, config);

    const newDisplay = MaskHelper.apply(inputEl.value, config.mask, config.tokens);

    if (newDisplay !== inputEl.value) {
        inputEl.value = newDisplay;
        const event = new Event('input');
        inputEl.dispatchEvent(event);
    }
};

function getInputElement(el: HTMLInputElement | HTMLElement): HTMLInputElement {
    if (el instanceof HTMLInputElement) {
        return el;
    }

    const elements = el.getElementsByTagName('input');

    if (elements.length !== 1) {
        throw new Error(`v-mask directive requires 1 input, found ${elements.length}`);
    }

    return elements[0];
}

function getConfig(bindingValue: string | string[] | VMaskConfig): VMaskConfig {
    let config: VMaskConfig = {
        mask: '',
        tokens: DEFAULT_MASK_TOKENS,
    };

    if (Array.isArray(bindingValue) || typeof bindingValue === 'string') {
        config.mask = bindingValue;
    } else {
        config = bindingValue;
    }

    return config;
}

function addOnInputHandler(inputEl: HTMLInputElement, config: VMaskConfig) {
    inputEl.oninput = function (evt: Event) {
        if (!evt.isTrusted) return;

        let position = inputEl.selectionEnd ?? 0;
        const digit = inputEl.value[position - 1];

        inputEl.value = MaskHelper.apply(inputEl.value, config.mask, config.tokens);

        while (position < inputEl.value.length && inputEl.value.charAt(position - 1) !== digit) {
            position++;
        }

        if (inputEl === document.activeElement) {
            inputEl.setSelectionRange(position, position);
            setTimeout(function () {
                inputEl.setSelectionRange(position, position);
            }, 0);
        }

        const event = new Event('input');
        inputEl.dispatchEvent(event);
    };
}
